using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using sso.Models;
using sso.Repositories;
using sso.Repositories.Interface;
using sso.Repositories.Model;
using sso.Services;

namespace sso.Controllers;

public class UserController : ControllerBase
{
    private readonly IUserRepository _userRepo;
    private readonly IPasswordHasher<UserModel> _pwdHasher;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly HttpContext _ctx;

    public UserController(IUserRepository userRepo, IPasswordHasher<UserModel> pwdHasher, IHttpContextAccessor httpContextAccessor)
    {
        _userRepo = userRepo;
        _pwdHasher = pwdHasher;
        _httpContextAccessor = httpContextAccessor;
        _ctx = _httpContextAccessor.HttpContext;
    }

    [HttpPost("register")]
    public async Task<IActionResult> Register([FromBody] UserModel user)
    {

        var entity = UserModel.To(user);
        entity.PasswordHash = _pwdHasher.HashPassword(user, user.Password);

        await _userRepo.RegisterUser(entity);

        await _ctx.SignInAsync(
            CookieAuthenticationDefaults.AuthenticationScheme,
            UserHelper.Convert(UserModel.From(entity))
        );

        return Ok(new
        {
            success = true
        });
    }

    [HttpPost("login")]
    public async Task<IActionResult> Login([FromBody] UserModel user)
    {
        var entity = UserModel.To(user);
        var existed = await _userRepo.GetUserByName(entity);
        var result = _pwdHasher.VerifyHashedPassword(new Models.UserModel()
        {
            UserName = existed.Username,
            Password = existed.PasswordHash
        }, existed.PasswordHash, user.Password);
        if (result == PasswordVerificationResult.Failed)
        {
            return NotFound();
        }
        await _ctx.SignInAsync(
            CookieAuthenticationDefaults.AuthenticationScheme,
            UserHelper.Convert(UserModel.From(entity))
        );

        return Ok(UserModel.From(existed));
    }
}