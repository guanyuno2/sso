﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using sso.Models;
using Microsoft.AspNetCore.Identity;

namespace sso.Data;

public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        // Customize the ASP.NET Identity model and override the defaults if needed.
        // For example, you can rename the ASP.NET Identity table names and more.
        // Add your customizations after calling base.OnModelCreating(builder);

        // Renaming the default ASP.NET Identity tables
        builder.Entity<ApplicationUser>().ToTable("Users"); // Renaming 'AspNetUsers' to 'MyUsers'
        builder.Entity<IdentityRole>().ToTable("Roles"); // Renaming 'AspNetRoles' to 'MyRoles'
        builder.Entity<IdentityUserRole<string>>().ToTable("UserRoles"); // Renaming 'AspNetUserRoles' to 'MyUserRoles'
        builder.Entity<IdentityUserClaim<string>>().ToTable("UserClaims"); // Renaming 'AspNetUserClaims' to 'MyUserClaims'
        builder.Entity<IdentityUserLogin<string>>().ToTable("UserLogins"); // Renaming 'AspNetUserLogins' to 'MyUserLogins'
        builder.Entity<IdentityRoleClaim<string>>().ToTable("RoleClaims"); // Renaming 'AspNetRoleClaims' to 'MyRoleClaims'
        builder.Entity<IdentityUserToken<string>>().ToTable("UserTokens"); // Renaming 'AspNetUserTokens' to 'MyUserTokens'

    }
}
