using System.ComponentModel.DataAnnotations;
using sso.Repositories.Model;
using sso.Services;

namespace sso.Models;

public class UserModel
{
    [Required]
    public string UserName { get; set; } = "";
    [Required]
    public string Password { get; set; } = "";
    [Required]
    [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
    public string ConfirmPassword { get; set; } = "";
    public List<UserClaimsModel> Claims { get; set; } = new List<UserClaimsModel>();

    public static UserModel From(MGUserModel model)
    {
        if (model == null) return null;
        return new UserModel()
        {
            UserName = model.Username
        };
    }

    public static MGUserModel To(UserModel model)
    {
        if (model == null) return null;
        return new MGUserModel()
        {
            Username = model.UserName
        };
    }
}