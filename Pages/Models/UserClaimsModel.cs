using System.ComponentModel.DataAnnotations;
using sso.Repositories.Model;
using sso.Services;

namespace sso.Models;
public class UserClaimsModel
{
    public string Type { get; set; }
    public string Value { get; set; }
}