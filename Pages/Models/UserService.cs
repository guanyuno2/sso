using System.Text;
using System.Text.Json;
using static System.Net.WebRequestMethods;
using Newtonsoft.Json;
using System.Net;
namespace sso.Models;

public class UserService
{
    private readonly HttpClient _httpClient;
    private readonly IHttpClientFactory _clientFactory;
    private readonly IConfiguration _config;
    public UserService(HttpClient httpClient, IHttpClientFactory clientFactory, IConfiguration config)
    {
        _httpClient = httpClient;
        _clientFactory = clientFactory;
        _config = config;
    }

    public async Task<UserModel> Register(UserModel user)
    {
        var uri = _config["Client"] + "register";

        var request = new HttpRequestMessage(HttpMethod.Post,
            uri);
        request.Content = new StringContent(System.Text.Json.JsonSerializer.Serialize(user), Encoding.UTF8, "application/json");

        using var client = _clientFactory.CreateClient("LocalApi");

        var response = await client.SendAsync(request);
        if (response.StatusCode == HttpStatusCode.OK)
        {
            var k = 0;
        }
        else
        {
            var k = 1;
        }


        return null;
    }

    public async Task<UserModel?> PostLogin(UserModel user)
    {
        var uri = _config["Client"] + "login";

        var request = new HttpRequestMessage(HttpMethod.Post,
            uri);
        request.Content = new StringContent(System.Text.Json.JsonSerializer.Serialize(user), Encoding.UTF8, "application/json");

        using var client = _clientFactory.CreateClient("LocalApi");

        var response = await client.SendAsync(request);
        if (response.StatusCode == HttpStatusCode.OK)
        {
            var responseData = await response.Content.ReadFromJsonAsync<UserModel>();
            var strResponse = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<UserModel>(strResponse);
        }
        else
        {
            Console.WriteLine($"HTTP Error: {response.StatusCode}");
        }
        return null;
    }
}