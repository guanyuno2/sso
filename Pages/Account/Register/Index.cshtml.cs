using Duende.IdentityServer;
using Duende.IdentityServer.Events;
using Duende.IdentityServer.Models;
using Duende.IdentityServer.Services;
using Duende.IdentityServer.Stores;
using Duende.IdentityServer.Test;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using sso.Models;

namespace sso.Pages.Register;

[SecurityHeaders]
[AllowAnonymous]
public class Index : PageModel
{
    private readonly IIdentityServerInteractionService _interaction;
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IEventService _events;

    [BindProperty]
    public InputModel Input { get; set; }
    public IList<AuthenticationScheme> ExternalLogins { get; set; }
    // Remaining API warnings ommited.
    public string ReturnUrl { get; set; }

    public Index(
        IIdentityServerInteractionService interaction,
        UserManager<ApplicationUser> userManager,
        IEventService events)
    {
        // this is where you would plug in your own custom identity management library (e.g. ASP.NET Identity)
        _userManager = userManager;
        _interaction = interaction;
        _events = events;
    }

    public IActionResult OnGet(string returnUrl = "/")
    {
        Input = new InputModel { };
        ReturnUrl = returnUrl;

        return Page();
    }

    public async Task<IActionResult> OnPost()
    {
        // check if we are in the context of an authorization request
        ReturnUrl ??= Url.Content("~/");

        var context = await _interaction.GetAuthorizationContextAsync(ReturnUrl);

        // the user clicked the "cancel" button
        if (Input.Button != "create")
        {
            if (context != null)
            {
                // if the user cancels, send a result back into IdentityServer as if they 
                // denied the consent (even if this client does not require consent).
                // this will send back an access denied OIDC error response to the client.
                await _interaction.DenyAuthorizationAsync(context, AuthorizationError.AccessDenied);

                // we can trust model.ReturnUrl since GetAuthorizationContextAsync returned non-null
                if (context.IsNativeClient())
                {
                    // The client is native, so this change in how to
                    // return the response is for better UX for the end user.
                    return this.LoadingPage(ReturnUrl);
                }

                return Redirect(ReturnUrl);
            }
            else
            {
                // since we don't have a valid context, then we just go back to the home page
                return Redirect("~/");
            }
        }

        var existedUser = await _userManager.FindByNameAsync(Input.Username);

        if (existedUser != null)
        {
            ModelState.AddModelError("Input.Username", "Existed username");
        }

        if (ModelState.IsValid)
        {
            var usr = new ApplicationUser
            {
                UserName = Input.Username,
                Email = Input.Email,
                EmailConfirmed = true,
            };
            var result = await _userManager.CreateAsync(usr, Input.Password);
            if (!result.Succeeded)
                throw new Exception(result.Errors.First().Description);

            var entity = await _userManager.FindByNameAsync(usr.UserName);

            await _events.RaiseAsync(new UserLoginSuccessEvent(entity.UserName, entity.Id, entity.UserName, clientId: context?.Client.ClientId));

            // await HttpContext.SignInAsync(isuser);

            if (context != null)
            {
                if (context.IsNativeClient())
                {
                    // The client is native, so this change in how to
                    // return the response is for better UX for the end user.
                    return this.LoadingPage(ReturnUrl);
                }

                // we can trust model.ReturnUrl since GetAuthorizationContextAsync returned non-null
                return Redirect(ReturnUrl);
            }

            // request for a local page
            if (Url.IsLocalUrl(ReturnUrl))
            {
                return Redirect(ReturnUrl);
            }
            else if (string.IsNullOrEmpty(ReturnUrl))
            {
                return Redirect("~/");
            }
            else
            {
                // user might have clicked on a malicious link - should be logged
                throw new Exception("invalid return URL");
            }
        }
        else
        {
            foreach (var key in ModelState.Keys)
            {
                var state = ModelState[key];
                if (state.Errors.Count > 0)
                {
                    // 'key' represents the field name and state.Errors contains the error messages
                    foreach (var error in state.Errors)
                    {
                        var errorMessage = error.ErrorMessage;
                        // Handle or log the error message for the specific field
                        Console.WriteLine("Model State not valid: " + errorMessage);
                    }
                }
            }
        }


        return Page();
    }
}