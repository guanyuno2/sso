
using System.ComponentModel.DataAnnotations;

namespace sso.Pages.Register;

public class InputModel
{
    [Required]
    [DataType(DataType.Text)]
    [Display(Name = "User Name")]
    public string Username { get; set; }

    [Required]
    [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
    [DataType(DataType.Password)]
    [Display(Name = "Password")]
    public string Password { get; set; }

    public string Name { get; set; }
    public string Email { get; set; }
    public string Button { get; set; }
}