using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Authentication.Cookies;
using sso.Models;

namespace sso.Services;
public class UserHelper
{
    public static ClaimsPrincipal Convert(UserModel model)
    {
        var claims = new List<Claim>(){
            new Claim("username", model.UserName)
        };
        claims.AddRange(model.Claims.Select(a => new Claim(a.Type, a.Value)));
        var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
        return new ClaimsPrincipal(identity);
    }
}