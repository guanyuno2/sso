using System.Security.Cryptography;
using System.Text;

namespace sso.Services;

public static class UserExtensions
{
    public static string UserHash(this string username) => Convert.ToBase64String(MD5.HashData(Encoding.UTF8.GetBytes(username)));
}