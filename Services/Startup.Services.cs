using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using MongoDB.Bson;
using MongoDB.Driver;
using sso.Models;
using sso.Repositories;
using sso.Repositories.Interface;

namespace sso.Services;

public static class StartupServices
{
    public static void AddCoreService(
                this IServiceCollection services, IConfiguration Configuration)
    {
        _ = services.AddSingleton<IMongoDb>(u =>
        {
            var mongoUrl = Configuration["Mongo"];
            var settings = MongoClientSettings.FromConnectionString(mongoUrl);
            // var mongoDb = (new MongoClient(mongoUrl)).GetDatabase(new MongoUrl(mongoUrl).DatabaseName);
            // return new MongoDb(mongoDb);
            // Set the ServerApi field of the settings object to Stable API version 1
            settings.ServerApi = new ServerApi(ServerApiVersion.V1);
            // Create a new client and connect to the server
            var client = new MongoClient(settings);
            // Send a ping to confirm a successful connection
            try
            {
                var result = client.GetDatabase("admin").RunCommand<BsonDocument>(new BsonDocument("ping", 1));
                Console.WriteLine("Pinged your deployment. You successfully connected to MongoDB!");
                return new MongoDb(client.GetDatabase("iofty"));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        });

        // services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IPasswordHasher<UserModel>, PasswordHasher<UserModel>>();
        
    }
}