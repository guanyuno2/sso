using sso.Repositories.Model;

namespace sso.Repositories.Interface;

public interface IUserRepository
{
    Task<MGUserModel> GetUserByName(MGUserModel user);
    Task RegisterUser(MGUserModel user);
}
