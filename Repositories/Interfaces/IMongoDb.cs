using MongoDB.Driver;

namespace sso.Repositories.Interface;

public interface IMongoDb
{
    IMongoDatabase Db { get; }
}