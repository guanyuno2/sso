using MongoDB.Bson;

namespace sso.Repositories.Interface;

public interface IBaseMongoEntity
{
    ObjectId Id { get; set; }
}