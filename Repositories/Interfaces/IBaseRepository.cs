
using System.Linq.Expressions;
using MongoDB.Bson;
using MongoDB.Driver;

namespace sso.Repositories.Interface;
public interface IBaseRepository<T>
{
    Task<T> GetById(ObjectId id);
    Task<T> GetById(string id);
    Task Add(T entity);
    Task Update(T entity, params Expression<Func<T, object>>[] properties);
    Task Delete(T entity);
}