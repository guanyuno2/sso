using MongoDB.Driver;

namespace sso.Repositories.Interface;
public class MongoDb : IMongoDb
{
    public MongoDb(IMongoDatabase db)
    {
        Db = db;
    }

    public IMongoDatabase Db { get; private set; }
}