using System.Linq.Expressions;
using System.Reflection;
using MongoDB.Bson;
using MongoDB.Driver;
using sso.Repositories.Interface;
using sso.Repositories.Model;

namespace sso.Repositories;

public class UserRepository : IUserRepository
{
    private readonly IMongoCollection<MGUserModel> _collection;
    public UserRepository(IMongoDb db)
    {
        _collection = db.Db.GetCollection<MGUserModel>(typeof(MGUserModel).Name);
    }

    public async Task<MGUserModel> GetUserByName(MGUserModel user)
    {
        var filter = Builders<MGUserModel>.Filter.And(
            Builders<MGUserModel>.Filter.Eq(a => a.Username, user.Username)
        );
        var data = await _collection.Find(filter).FirstOrDefaultAsync();
        return data;
    }

    public Task RegisterUser(MGUserModel entity)
    {
        if (entity.Id == null)
            entity.Id = ObjectId.GenerateNewId();
        return _collection.InsertOneAsync(entity);
    }
}