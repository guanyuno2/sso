using System.Linq.Expressions;
using System.Reflection;
using MongoDB.Bson;
using MongoDB.Driver;
using sso.Repositories.Interface;

namespace sso.Repositories;

public abstract class BaseRepository<T> : IBaseRepository<T> where T : class, IBaseMongoEntity
{
    private readonly IMongoCollection<T> _collection;
    public BaseRepository(IMongoDatabase db)
    {
        _collection = db.GetCollection<T>(typeof(T).Name);
    }


    public Task Add(T entity)
    {
        if (entity.Id == null)
            entity.Id = ObjectId.GenerateNewId();
        return _collection.InsertOneAsync(entity);
    }

    public Task Delete(T entity)
    {
        if ((object)entity == null)
            throw new KeyNotFoundException();
        var filter = Builders<T>.Filter.Where(m => m.Id == entity.Id);

        return _collection.DeleteOneAsync(filter);
    }

    public Task<T> GetById(ObjectId id)
    {
        if (id == null)
            throw new ArgumentException(nameof(id));

        var filter = Builders<T>.Filter.Where(m => m.Id == id);

        var data = _collection.Find(filter).FirstOrDefaultAsync();
        return data;
    }

    public Task<T> GetById(string id)
    {
        if (id == null)
            throw new ArgumentException(nameof(id));
        var oid = ObjectId.Parse(id);
        var filter = Builders<T>.Filter.Where(m => m.Id == oid);

        var data = _collection.Find(filter).FirstOrDefaultAsync();
        return data;
    }

    public Task Update(T entity, params Expression<Func<T, object>>[] properties)
    {
        ObjectId id = entity.Id;
        var filter = Builders<T>.Filter.Where(m => m.Id == entity.Id);

        // entity.UpdatedAt = DateTime.UtcNow;

        if (properties == null || properties.Length == 0)
            return _collection.ReplaceOneAsync(filter, entity);

        UpdateDefinition<T> update = (UpdateDefinition<T>)null;
        Type type = typeof(T);
        foreach (Expression<Func<T, object>> property in properties)
        {
            string propertyName = property.GetPropertyName<T, object>();
            if (propertyName == "UpdatedAt")
                update = __invoke(propertyName, DateTime.UtcNow, update);
            else if (propertyName == "UpdatedBy")
                update = __invoke(propertyName, 0, update);
            else
                update = __invoke(propertyName, type.GetProperty(propertyName).GetValue((object)entity), update);
        }

        return _collection.UpdateOneAsync(filter, update);
    }

    private UpdateDefinition<T> __invoke(string propertyName, object value, UpdateDefinition<T> update)
    {
        Type type = typeof(object);
        if (value != null)
            type = value.GetType();
        return (UpdateDefinition<T>)GetType().GetMethod(
            "__set",
            BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .MakeGenericMethod(type).Invoke(
                    (object)this,
                    new object[3]
                    {
                            (object) propertyName,
                            value,
                            (object) update
                    });
    }
}

public static class RepositoryExtension
{
    public static string GetPropertyName<T, TProperty>(this Expression<Func<T, TProperty>> expression)
    {
        MemberExpression memberExpression = null;
        if (expression.Body is UnaryExpression)
            memberExpression = (MemberExpression)((UnaryExpression)expression.Body).Operand;
        if (expression.Body is MemberExpression)
            memberExpression = expression.Body as MemberExpression;
        if (memberExpression == null)
            throw new InvalidOperationException("Expression must be a member expression");
        return memberExpression.Member.Name;
    }
}